package com.modernetic.personapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.modernetic.personapi.models.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{
    
}
