package com.modernetic.personapi.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modernetic.personapi.models.Person;
import com.modernetic.personapi.repositories.PersonRepository;

@RestController
@RequestMapping("api/persons")
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @GetMapping("/")
    public ResponseEntity<List<Person>> getAllPerson() {
        try {
            List<Person> person = new ArrayList<Person>();
            personRepository.findAll().forEach(person::add);
            // else
            // personRepository.findByTitleContaining(title).forEach(persons::add);
            if (person.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(person, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id) {
        Optional<Person> personData = personRepository.findById(id);
        if (personData.isPresent()) {
            return new ResponseEntity<>(personData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/")
    public ResponseEntity<Person> createPerson(@RequestBody Person person) {
        try {
            Person _person = personRepository
                    .save(person);
            return new ResponseEntity<>(_person, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/")
    public ResponseEntity<Person> updatePerson(@PathVariable("id") Long id,
            @RequestBody Person person) {
        Optional<Person> personData = personRepository.findById(id);
        if (personData.isPresent()) {
            Person _person = personData.get();
            _person.setFirst_name(person.getFirst_name());
            _person.setLast_name(person.getLast_name());
            _person.setSexe(person.getSexe());
            _person.setAge(person.getAge());
            return new ResponseEntity<>(personRepository.save(_person), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<HttpStatus> deletePerson(@PathVariable("id") Long id) {
        try {
            personRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
   
    
}
