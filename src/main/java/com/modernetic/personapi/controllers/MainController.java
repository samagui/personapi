package com.modernetic.personapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping("/")
    public String home(){
        return """
            <h1> Welcome to Spring Boot App deployment on render.com CodeLab </h1><br/>
            <h4> Clic <a href="swagger-ui.html">here</a> to see the Rest Api docs</h4>
        """;
    }
    
}
